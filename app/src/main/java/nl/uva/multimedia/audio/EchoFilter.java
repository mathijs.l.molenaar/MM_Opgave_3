package nl.uva.multimedia.audio;

/* 
 * Framework for audio playing, visualization and filtering
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

public class EchoFilter implements Filter {
    private CircularBuffer buffer;

    public EchoFilter(int size) {
        buffer = new CircularBuffer(size);
    }

    public void filter(short[] samples, int length) {
        /* Add the samples to the circular buffer. */
        for (short sample : samples) {
            buffer.enqueue(sample);
        }

        /*
         * Check if there is enough room for another set of samples after this one.
         * If there is not enough room anymore we cant start dequeing and mixing saved samples with the current samples.
         *
         * Check for almost full instead of completely full so it can handle calculation errors (if the buffer size isn't a
         *  multiple of the amount of samples we get per time (the length variable).
         */
        if (buffer.isAlmostFull(length)) {
            for (int i = 0; i < length; i++) {
                samples[i] = (short) (samples[i] / 2 + buffer.dequeue() / 4);
            }
        }
    }
}
