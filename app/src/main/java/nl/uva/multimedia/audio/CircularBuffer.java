package nl.uva.multimedia.audio;

/**
 * Created by Mathijs on 10/06/14.
 */
public class CircularBuffer {
    private int front = 0;// First item in the queue
    private int end = 0;// Last item in the queue
    private int itemCount = 0;
    private short[] array;

    public CircularBuffer(int size) {
        array = new short[size];
    }

    /*
     * Enqueue an item
     */
    public boolean enqueue(short value) {
        if (itemCount == array.length)
            return false;

        array[end] = value;
        end = (end+1)%array.length;
        itemCount++;
        return true;
    }

    /*
     * Dequeue an item
     */
    public short dequeue() {
        if (itemCount == 0)
            return 0;

        short tmp =  array[front];
        front =  (front+1)%array.length;
        itemCount--;
        return tmp;
    }

    /*
     * Check if the queue is full
     */
    public boolean isAlmostFull(int neededSize) {
        return itemCount+neededSize > array.length;
    }
}
