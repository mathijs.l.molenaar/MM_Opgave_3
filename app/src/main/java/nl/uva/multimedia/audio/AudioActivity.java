package nl.uva.multimedia.audio;

/* 
 * Framework for audio playing, visualization and filtering
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


public class AudioActivity extends Activity {
    private MySlider mySlider;
    private MySwitch mySwitch;
    PlaybackManager playbackManager;

    boolean useMic = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        mySlider = (MySlider)findViewById(R.id.mySlider);
        mySwitch = (MySwitch)findViewById(R.id.mySwitch);

        playbackManager = new PlaybackManager(this);

        playbackManager.setPauseButton((PauseButton)findViewById(R.id.pausebutton));
        playbackManager.setPlayButton((PlayButton)findViewById(R.id.playbutton));
        playbackManager.setStopButton((StopButton)findViewById(R.id.stopbutton));
        playbackManager.setAudioSourceSwitch((AudioSourceSwitch)findViewById(R.id.audioSourceSwitch));

        playbackManager.setVisiblity(PlaybackManager.STATE_STOPPED);


        mySlider.setMin(1);
        mySlider.setMax(100);
        mySlider.setPbManager(playbackManager);

        mySwitch.setMySlider(mySlider);
    }

    /* This method is called when the audio file chooser activity has a result
     * and returns to this activity.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
         * Removed ton of useless code which didn't even work on KitKat because of the new Storage Access Framework.
         * Instead of getting the path we just use with the URI like we're supposed to.
         */
        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();

			/* Check that the file is a WAVE-file and not an mp3-file */
            if (!WaveFile.isWaveFile(uri, getApplicationContext())) {
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .create();
                alertDialog.setTitle("Invalid file-type");
                alertDialog.setMessage("The selected file is not a valid wave file.");
                alertDialog.show();
                playbackManager.setAudioSource(PlaybackManager.SOURCE_MIC);
            } else {
                playbackManager.setFileSource(uri);
            }
        } else {
            playbackManager.setAudioSource(PlaybackManager.SOURCE_MIC);
            return;
        }
    }

    /* Stop playing when the application is in the background. */
    @Override
    public void onStop() {
        super.onStop();
        playbackManager.stopPlaying();
    }
}
