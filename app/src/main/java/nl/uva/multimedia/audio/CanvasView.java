package nl.uva.multimedia.audio;

/* 
 * Framework for audio playing, visualization and filtering
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class CanvasView extends View {

    public CanvasView(Context context) {
        super(context);
    }

    public CanvasView(Context context, AttributeSet attributes) {
        super(context, attributes);
    }

    public CanvasView(Context context, AttributeSet attributes, int style) {
        super(context, attributes, style);
    }

    @Override
    protected void onMeasure(int width, int height) {
        super.onMeasure(width, height);
    }

    /* Called whenever the canvas is dirty and needs redrawing */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(Color.BLACK);

		/* Define the basic paint */
        Paint paint = new Paint();
        paint.setColor(Color.rgb(0xa0, 0xa0, 0xb0));
        paint.setAntiAlias(true);
	
		/* text inherits from the basic paint */
        Paint text = new Paint(paint);
        text.setColor(Color.WHITE);
        text.setShadowLayer(3.0F, 3.0F, 3.0F, Color.rgb(0x20, 0x20, 0x20));
        text.setTextSize(getHeight() * 0.1F);
	
		/* Save state */
        canvas.save();
        canvas.translate(getWidth() * 0.1F, getHeight() * 0.1F);

        canvas.drawText("Hello world! ", 0.0F, 0.0F, text);

        canvas.restore();
    }

}