package nl.uva.multimedia.audio;

/* 
 * Framework for audio playing, visualization and filtering
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.Toast;

class MySlider extends SeekBar implements SeekBar.OnSeekBarChangeListener {
    private int min;
    private PlaybackManager pbManager;
    private Toast toast;

    /* Necessary constructors */
    public MySlider(Context context) {
        super(context);
        setup();
    }

    public MySlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public MySlider(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup();
    }

    private void setup() {
        setProgress(10);
        setEnabled(false);
        setOnSeekBarChangeListener(this);
        toast = Toast.makeText(getContext().getApplicationContext(), "", Toast.LENGTH_SHORT);
    }


    public void onProgressChanged(SeekBar slider, int progress, boolean from_user) {
        progress += min;

        if (pbManager != null) {
            setFilter(progress);
        } else {
            Log.e("MySlider", "No pb manager or audioplayer");
        }
    }

    public void setFilter(int progress) {
        pbManager.setEchoTime(progress);

            /*
             * Set toast as a class variable so it can be updated when the user is scrolling (instead of showing all
             *  toasts one after eachother).
             */
        toast.setText(String.valueOf((float)progress / 10) + " seconden vertraging/echo");
        toast.show();
    }

    public void onStartTrackingTouch(SeekBar slider) { /* NOP */ }

    public void onStopTrackingTouch(SeekBar slider) { /* NOP */ }

    public void setMax(int max) { super.setMax(max - min); }
    public void setMin(int min) { this.min = min; }
    public void setPbManager(PlaybackManager pbManager) { this.pbManager = pbManager; }
    public PlaybackManager getPbManager() { return this.pbManager; }
}
