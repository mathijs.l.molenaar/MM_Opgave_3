package nl.uva.multimedia.audio;

/* 
 * Framework for audio playing, visualization and filtering
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

import android.content.Context;
import android.media.AudioFormat;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

public class WaveFile {
    private InputStream file;
    private short numChannels;
    private int sampleRate;
    private int byteRate;
    private short bitsPerSample;
    private int bytesToRead;

    private byte[] byteBuffer;

	/* XXX Declare your variables here. */

    public WaveFile(Uri uri, Context context) {
        try {
            file = context.getContentResolver().openInputStream(uri);
            /* Please use the FileInputStream and not the Buffered variant of it.
             * See the Java docs how to use it. */

            /*
             * Read header file and save the important information.
             * Skip some extra bytes if the data doesnt start on its usual location (because of the optional parameters: ExtraParams).
             */
            byte[] buffer = new byte[100];

            /*
             * Lees alle chunks en haal de nodige informatie er uit.
             * Stop wanneer het datablok gevonden is.
             * Om er 100 procent zeker van te zijn dat we niet te veel lezen wordt slaan we de lengte van het datablok op.
             * Hierdoor werkt het programma ook correct als er nog meer chunks zijn na het data chunk (hoewel die chunks dan niet gelezen worden).
             * Uit het "fmt " blok wordt de nodige informatie over de geluidsdata opgehaald en opgeslagen.
             */
            int i = 0;
            while (i++ < 100) {
                file.read(buffer, 0, 8);
                String chunkId = bytesToString(buffer, 0, 4);
                int chunkSize = bytesToInt(buffer, 4, 4);
                if (chunkId.equals("data")) {
                    bytesToRead = chunkSize;
                    Log.v("WaveFile", "Datablok found, bytes to read: " + bytesToRead);
                    break;
                } else if (chunkId.equals("fmt ")) {
                    file.read(buffer, 0, chunkSize);
                    numChannels = bytesToShort(buffer, 2);
                    sampleRate = bytesToInt(buffer, 4, 4);
                    byteRate = bytesToInt(buffer, 8, 4);
                    bitsPerSample = bytesToShort(buffer, 14);
                    Log.v("WaveFile", "Channels: " + numChannels + "; Samplerate: " + sampleRate + "'Bits per sample: " + bitsPerSample);
                } else if (chunkId.equals("RIFF")) {
                    /*
                     * De ChunkSize is het aantal bytes tot het einde van de file, niet het einde van de chunk.
                     * Het RIFF chunk is altijd 12 bytes lang (inclusief ChunkId en ChunkSize) dus we moeten nog 4 bytes lezen.
                     */
                    file.skip(4);
                } else {
                    Log.v("WaveFile", chunkId + " not recignized correct block, skipping " + chunkSize + " to next block");
                    file.skip(chunkSize);
                }
            }
            //Log.e("WaveFile", bytesToString(buffer, 0, 100));
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("WaveFile", "Something went wrong..");
            return;
        }
    }

    public static boolean isWaveFile(Uri uri, Context context) {
		/*
		 * Check if a file is a wave file that we can read/play.
		 * We only support uncompressed wave files (PCM).
		 * Wave is part of RIFF (by Microsoft).
		 */
        InputStream testFile = null;
        byte[] buffer = new byte[36];
        try {
            testFile = context.getContentResolver().openInputStream(uri);
            testFile.read(buffer, 0, 36);
            if (!bytesToString(buffer, 0, 4).equals("RIFF")) {
                testFile.close();
                Log.e("WaveFile", "Geen RIFF, maar: " + bytesToString(buffer, 0, 4));
                return false;
            }

            if (!bytesToString(buffer, 8, 4).equals("WAVE")) {
                testFile.close();
                Log.e("WaveFile", "Geen WAVE, maar: " + bytesToString(buffer, 8, 4));
                return false;
            }

            if (bytesToShort(buffer, 20) != 1) {
                testFile.close();
                Log.e("WaveFile", "Niet PCM");
                return false;
            }

            testFile.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

		/* XXX Extra: determine if a valid wave-file can be played with Android
		 * (surround sound, etc..)
		 */
        return true;
    }

    /* Convert bytes to an integer (bytes assumed to be in little endian order). */
    private static int bytesToInt(byte[] bytes, int offset, int length) {
        int result = 0;
        for (int i = 0; i < length; i++)
            result += (bytes[i + offset] & 0xFF) << i * 8;
        return result;
    }

    /* Convert bytes to a string (bytes assumed to be in big endian order). */
    private static String bytesToString(byte[] bytes, int offset, int length) {
        char[] string = new char[length];

        for (int i = 0; i < length; i++)
            string[i] += (char) bytes[i + offset];

        return new String(string);

    }

    /* Bytes to short, always uses 2 bytes (assumes little endian). */
    private static short bytesToShort(byte[] bytes, int offset) {
        return (short) (((bytes[offset + 1] & 0xFF) << 8) + (bytes[offset] & 0xFF));
    }

    public boolean getData(short[] shortBuffer, int bufferSizeInBytes) {
		/* Re-use the bytebuffer for efficient memory usage! */
        if (byteBuffer == null)
            byteBuffer = new byte[bufferSizeInBytes];

		/*
		 * XXX Read the file into the byteBuffer, and put the bytes into the
		 * given shortBuffer (please think about the 8 or 16-bit encoding of
		 * a wave file).
		 *
		 * Return false when done reading or when an error occurs.
		 */
        try {
            if (file.read(byteBuffer, 0, bufferSizeInBytes) != bufferSizeInBytes || bytesToRead <= 0) {
                /* End of file reached */
                file.close();
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        bytesToRead -= bufferSizeInBytes;

        if (bitsPerSample == 8) {
            for (int i = 0; i < bufferSizeInBytes; i++) {
                shortBuffer[i] = (short)((((short)byteBuffer[i])-128)<<8);
                /*Log.v("WaveFile", String.valueOf((byteBuffer[i]&0x01)==0?0:1) +
                        String.valueOf((byteBuffer[i]&0x02)==0?0:1) +
                        String.valueOf((byteBuffer[i]&0x04)==0?0:1) +
                        String.valueOf((byteBuffer[i]&0x08)==0?0:1) +
                        String.valueOf((byteBuffer[i]&0x10)==0?0:1) +
                        String.valueOf((byteBuffer[i]&0x20)==0?0:1) +
                        String.valueOf((byteBuffer[i]&0x40)==0?0:1) +
                        String.valueOf((byteBuffer[i]&0x80)==0?0:1) +
                        " wordt: " + shortBuffer[i]);*/
            }
            return true;
        } else if (bitsPerSample == 16) {
            /* Every 2 bytes go into 1 short (so the short array is half the length of the byte array) */
            int length = bufferSizeInBytes/2;
            for (int i = 0; i < length; i++) {
                shortBuffer[i] = bytesToShort(byteBuffer, i<<1);// i<<2 = i*2
            }
            return true;
        }

        return false;
    }
	
	/* XXX Please use the functions below instead of slow ByteBuffer objects :) */

    /*
     * Return the sampleRate in Hz of the wave-file
     */
    public int getSampleRate() {
        if (bitsPerSample == 8)
            return sampleRate/2;
        else
            return sampleRate;
    }

    public int getByteRate() { return byteRate; }

    /*
     * Return AudioFormat.ENCODING_PCM_16BIT even if the file is 8 bits because we do the conversion manually
     */
    public int getAudioFormat() {
        if (bitsPerSample == 8 || bitsPerSample == 16)
            return AudioFormat.ENCODING_PCM_16BIT;

        else
            return AudioFormat.ENCODING_INVALID;
    }

    /*
     * Return AudioFormat.CHANNEL_OUT_STEREO or AudioFormat.CHANNEL_OUT_MONO
     * depending on the file..
     */
    public int getChannelConfig() {
        if (numChannels == 1)
           return AudioFormat.CHANNEL_OUT_MONO;
        else if (numChannels == 2)
            return AudioFormat.CHANNEL_OUT_STEREO;
        else if (numChannels == 4) {
            return AudioFormat.CHANNEL_OUT_QUAD;
        } else if (numChannels == 6) {
            return AudioFormat.CHANNEL_OUT_5POINT1;
        } else if (numChannels == 8) {
            return AudioFormat.CHANNEL_OUT_7POINT1;
        } else {
            return -1;
        }
    }
}